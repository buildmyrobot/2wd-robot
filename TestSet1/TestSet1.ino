// This script is used to test the robot functionality
// There is no intelligence built into this script
// Keyboard keys aswd control the movement in 4 directions
// The serial port reads out the sensor status
// Author: william@machineiq.com.au
// Date: 26 January 2014


// Here we define which pins are connected to the sensors and the motors: 
// Motor 1 is connected to the left hand drive wheel
int left_motor_direction = 4;
int left_motor_speed = 5;
// Motor 2 is connected to the right hand drive wheel
int right_motor_direction = 7;
int right_motor_speed = 6;

// The left line sensor is connected to input number 9
int line_sensor_left = 10;
// The left line sensor is connected to input number 10
int line_sensor_centre = 11;
// The right hand line sensor is connected to input number 11
int line_sensor_right = 12;

// Define directions
int FORWARD = 1;
int BACKWARD = 0;

void setup() {
  // This code runs once when the robot starts

  // Set the pins connected to the motors to outputs
  pinMode(right_motor_direction, OUTPUT);
  pinMode(right_motor_speed, OUTPUT);
  pinMode(right_motor_direction, OUTPUT);
  pinMode(right_motor_speed, OUTPUT);
  // Define the initial conditions for the pins to be 0's
  digitalWrite(left_motor_direction, 0);
  digitalWrite(left_motor_speed, 0);
  digitalWrite(right_motor_direction,0);
  digitalWrite(right_motor_speed,0);
  
  // Set the sensors to inputs
  pinMode(line_sensor_left, INPUT);
  pinMode(line_sensor_right, INPUT);
  pinMode(line_sensor_centre, INPUT);
  // Setup the serial port to talk to the PC at 57600 baud 8-n-1 (Default)
  Serial.begin(57600);
  
  while(!Serial)
  {
    ; // Wait while the serial port initializes
  }
  
  // Print a greeting to the computer
  Serial.println("::BuildMyRobot::");
}

void loop() {
  // put your main code here, to run repeatedly: 
     if(Serial.available())
     {
      char val = Serial.read();
      if(val != -1)
      {
        switch(val)
        {
        case 'w'://Move Forward
          advance ();   //move forward in 1/2 speed
          print_sensors();
          break;
        case 's'://Move Backward
          back_off ();   //move back in 1/2 speed
          print_sensors();
          break;
        case 'a'://Turn Left
          turn_left ();    // turn left 1/2 speed
          print_sensors();
          break;       
        case 'd'://Turn Right
          turn_right ();    // turn right 1/2 speed
          print_sensors();
          break;
        case 'z':              // If we need to check the micro is responding
          Serial.println("Hello");
          print_sensors();
          break;
        case 'x':             // When we want to stop the robot
          stop();
          print_sensors();
          break;
        default:              // Default action is to cause a stop
          stop();
          print_sensors();
          break;
        }
     }
    
    } 
}

void stop(void)
{
  // Set both motor speeds to 0
  digitalWrite(left_motor_speed, 0);
  digitalWrite(right_motor_speed,0);
}

void advance(void)
{
  // Stop both motors
  analogWrite(left_motor_speed, 0);
  analogWrite(right_motor_speed, 0);
  // Set both motors to turn forwards
  digitalWrite(left_motor_direction, FORWARD);
  digitalWrite(right_motor_direction, FORWARD);
  // Set both motors to the same speed to go forwards
  // The minimum the motors require to turn is approximately 80
  // The maximum you can give the motors is 255
  analogWrite(left_motor_speed, 100);
  analogWrite(right_motor_speed, 100);
}

void back_off(void)
{
  // Stop both motors
  analogWrite(left_motor_speed, 0);
  analogWrite(right_motor_speed, 0);
  // Set both motors to turn backwards
  digitalWrite(left_motor_direction, BACKWARD);
  digitalWrite(right_motor_direction, BACKWARD);
  // Set both motors to the same speed to go forwards
  // The minimum the motors require to turn is approximately 80
  // The maximum you can give the motors is 255
  analogWrite(left_motor_speed, 100);
  analogWrite(right_motor_speed, 100);
}

void turn_left(void)
{
  // Stop both motors
  analogWrite(left_motor_speed, 0);
  analogWrite(right_motor_speed, 0);
  // Set the left motor to turn backwards and the right motor to turn fowards
  digitalWrite(left_motor_direction, BACKWARD);
  digitalWrite(right_motor_direction, FORWARD);
  // Set both motors to turn at the same speed
  analogWrite(left_motor_speed, 100);
  analogWrite(right_motor_speed, 100);
  // Remember that although the speeds are the same, the directions are different
}

void turn_right(void)
{
  // Stop both motors
  analogWrite(left_motor_speed, 0);
  analogWrite(right_motor_speed, 0);
  // Set the right motor to turn backwards and the left motor to turn fowards
  digitalWrite(right_motor_direction, BACKWARD);
  digitalWrite(left_motor_direction, FORWARD);
  // Set both motors to turn at the same speed
  analogWrite(left_motor_speed, 100);
  analogWrite(right_motor_speed, 100);
  // Remember that although the speeds are the same, the directions are different
}

void print_sensors(void)
{
  Serial.print("L: ");
  Serial.print(digitalRead(line_sensor_left));
   Serial.print(" C: ");
  Serial.print(digitalRead(line_sensor_centre));
  Serial.print(" R: ");
  Serial.println(digitalRead(line_sensor_right));
}
